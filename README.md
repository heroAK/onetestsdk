# OneTestSDK

<<<<<<< HEAD
[![CI Status](https://img.shields.io/travis/wangtotang/OneTestSDK.svg?style=flat)](https://travis-ci.org/wangtotang/OneTestSDK)
[![Version](https://img.shields.io/cocoapods/v/OneTestSDK.svg?style=flat)](https://cocoapods.org/pods/OneTestSDK)
[![License](https://img.shields.io/cocoapods/l/OneTestSDK.svg?style=flat)](https://cocoapods.org/pods/OneTestSDK)
[![Platform](https://img.shields.io/cocoapods/p/OneTestSDK.svg?style=flat)](https://cocoapods.org/pods/OneTestSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OneTestSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'OneTestSDK'
```

## Author

wangtotang, 2387825290@qq.com

## License

OneTestSDK is available under the MIT license. See the LICENSE file for more info.


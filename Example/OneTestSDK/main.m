//
//  main.m
//  OneTestSDK
//
//  Created by wangtotang on 05/17/2019.
//  Copyright (c) 2019 wangtotang. All rights reserved.
//

@import UIKit;
#import "BTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BTAppDelegate class]));
    }
}

//
//  BTAppDelegate.h
//  OneTestSDK
//
//  Created by wangtotang on 05/17/2019.
//  Copyright (c) 2019 wangtotang. All rights reserved.
//

@import UIKit;

@interface BTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
